import * as dotenv from "dotenv";
dotenv.config();

import { BITBOX as BITBOXSDK, ECPair } from "bitbox-sdk";
const BITBOX = new BITBOXSDK();

const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

const difficulty = parseInt(process.env.MINER_DIFFICULTY_V1 as string, 10);

export const workerThread = async (worker: any) => {
    //set up event handler for receiving work from master
    worker.on('message', async (message: any) => {
        let solhash = message.solhash;
        let scriptPreImage = message.scriptPreImage;
        let prehash = message.prehash;

        // find solution
        while (!solhash.slice(0, difficulty).toString("hex").split("").every((s: string) => s === "0")) {
            prehash[0 + scriptPreImage.length] = Math.floor(Math.random() * 255);
            prehash[1 + scriptPreImage.length] = Math.floor(Math.random() * 255);
            prehash[2 + scriptPreImage.length] = Math.floor(Math.random() * 255);
            prehash[3 + scriptPreImage.length] = Math.floor(Math.random() * 255);
            solhash = BITBOX.Crypto.hash256(prehash);

        }

        // send master our solution
        worker.send({solhash, scriptPreImage, prehash});

    });

    // we will find solution and exit before timer expires
    await sleep(600000);
};
