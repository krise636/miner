import * as dotenv from "dotenv";
dotenv.config();
import BigNumber from "bignumber.js";
import { BITBOX as BITBOXSDK, ECPair } from "bitbox-sdk";
import * as crypto from "crypto";
import EventSource from "eventsource";
import fetch from "node-fetch";
import { BchdNetwork, LocalValidator,
         ScriptSigP2PK, ScriptSigP2PKH, ScriptSigP2SH,
         Slp, SlpAddressUtxoResult, TransactionHelpers,
         Utils} from "slpjs";

// JSON-RPC
let useRpc = (process.env.USE_RPC as string == "true") ? true: false;
const rpcClient = require('bitcoin-promise');
const rpc = new rpcClient.Client({
    host:process.env.RPC_HOST as string,
    port:process.env.RPC_PORT as string,
    user:process.env.RPC_USER as string,
    pass:process.env.RPC_PASS as string,
    timeout:5000
});

const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

const BITBOX = new BITBOXSDK();

const slp = new Slp(BITBOX);
const txnHelpers = new TransactionHelpers(slp);

import bchaddr from "bchaddrjs-slp";
const Bitcore = require("bitcoincashjs-lib-p2sh");

import { GrpcClient } from "grpc-bchrpc-node";
const client = new GrpcClient();

// multiple cpu
const cluster = require("cluster");
const minerCpuCount = parseInt(process.env.CPU_COUNT as string);

const minerWif: string = process.env.WIF!;
const minerPubKey = (new ECPair().fromWIF(minerWif)).getPublicKeyBuffer();
const minerBchAddress = Utils.toCashAddress((new ECPair().fromWIF(minerWif)).getAddress());
const minerSlpAddress = Utils.toSlpAddress(minerBchAddress);

const vaultHexTail = process.env.MINER_COVENANT_V1!;
const TOKEN_START_BLOCK = parseInt(process.env.TOKEN_START_BLOCK_V1 as string, 10);
const difficulty = parseInt(process.env.MINER_DIFFICULTY_V1 as string, 10);

export class TxCache {
    public static txCache = new Map<string, Buffer>();
}

const fs = require("fs");
const jsonFile = require("jsonfile");

const doesTxsFileExist = () => fs.existsSync("./txs.json");
const writeJsonToFile = (jsonStr: string) => jsonFile.writeFileSync('./txs.json', JSON.parse(jsonStr));
const readTxsToJsonString = () => fs.readFileSync('./txs.json');

const mapToJson = (map: Map<string, Buffer>) => {
    let jsonObject: any = {};  
    map.forEach((value, key) => {  
       jsonObject[key] = value  
    });
    let json = <JSON>jsonObject;
    return JSON.stringify(json);
}

const jsonToMap = (jsonStr: string) => {
    let jsonObj = JSON.parse(jsonStr);
    let map = new Map<string, Buffer>();
    for (let key in jsonObj) {
        let val = jsonObj[key];
        var buf = Buffer.from(val["data"]);
        map.set(key, buf);
    }
    return map;
}

// Load TxCache from file
console.log("Loading TxCache from JSON file...");
TxCache.txCache = jsonToMap(readTxsToJsonString());
console.log("Done");

/**
 *    sse is broken... don't use
 *
 */
/*
// listen for new mints
let mintFound = false;
const sseMintQuery = {
    v: 3,
    "q": {
        find: {
            "slp.valid": true,
            "slp.detail.transactionType": "MINT",
            "slp.detail.tokenIdHex": process.env.TOKEN_ID_V1,
        },
    },
};
const sseb64 = Buffer.from(JSON.stringify(sseMintQuery)).toString("base64");
const sse = new EventSource(process.env.SLPSOCKET_URL + sseb64);
sse.onmessage = (e: any) => {
    const data = JSON.parse(e.data);
    if (data.type !== "mempool" && data.type !== "block" || data.data.length < 1) {
        return;
    }
    console.log("sse reported new MIST MINT");
    mintFound = true;
};
*/
// setup a new local SLP validator
const validator = new LocalValidator(BITBOX, async (txids) => {
        let txnBuf;
        try {
            if (TxCache.txCache.has(txids[0])) {
                // console.log(`Cache txid: ${txids[0]}`);
                return [ TxCache.txCache.get(txids[0])!.toString("hex") ];
            }
            console.log(`Downloading txid: ${txids[0]}`);
            let res: any = undefined;
            /**
             * sometimes this loop gets stuck forever because SLPDB will give us a txid
             * that does not exist on the BCHD host
             *
             * the workaround is to use SLPDB that is connected to the same BCHD host
             */
            while(!res) {
                try {
                    res = await client.getRawTransaction({ hash: txids[0], reversedHashOrder: true });
                } catch (e) {
                    // console.log(`${txids}[0] failed: ${e}`);
                    await sleep(250);
                }
            }
            
            txnBuf = Buffer.from(res.getTransaction_asU8());
            TxCache.txCache.set(txids[0], txnBuf);
        } catch (err) {
            throw Error(`[ERROR] Could not get transaction ${txids[0]} in local validator: ${err}`);
        }
        return [ txnBuf.toString("hex") ];
    },
);
const network = new BchdNetwork({BITBOX, client, validator});

const getRewardAmount = (block: number) => {
    const initReward = parseInt(process.env.TOKEN_INIT_REWARD_V1 as string, 10);
    const halveningInterval = parseInt(process.env.TOKEN_HALVING_INTERVAL_V1 as string, 10);
    return Math.floor(initReward / (Math.floor(block / halveningInterval) + 1));
};

// realtime block updates
const useZeromq: boolean = (process.env.USE_ZEROMQ as string) == "true"? true: false;
const zeromq = require('zeromq');
let sock = zeromq.socket('sub');
if(useZeromq) {
    let zeromqHost = process.env.ZEROMQ_HOST as string;
    let zeromqPort = process.env.ZEROMQ_PORT as string;
    // one TCP connection to zmq host
    sock.connect(`tcp://${zeromqHost}:${zeromqPort}`);
    sock.subscribe('hashblock');    
}

// true when worker finds solution
let solhashFound: boolean = false;
// true when zeromq finds new block
let bchBlockFound = false;
// store previous lastBatonTxid value to compare against SLPDB query
// makes sure we don't mine for solution already found
let prev_lastBatonTxid: any;

export const generateV1 = async ({ lastBatonTxid, mintVaultAddressT0 }: { lastBatonTxid?: string, mintVaultAddressT0?: string }) => {

    // clear all zeromq listeners
    sock.removeAllListeners();

    // reset vars
    bchBlockFound = false;
    solhashFound = false;
    // mintFound = false; // not using sse

    // find lastest unspent baton transaction using slpdb
    let batonInputIndex = 0;
    let bchBlockHeight: number;

    const batonQuery = {
        v: 3,
        "q": {
            db: ["g"],
            "aggregate": [
                { $match: {
                    "tokenDetails.tokenIdHex": process.env.TOKEN_ID_V1 as string,
                    "graphTxn.outputs": {$elemMatch: {status: "BATON_UNSPENT"}}},
                },
                { $project: { graphTxn: 1, context: "SLPDB" }},
                {
                    $lookup: {
                        from: "statuses",
                        localField: "context",
                        "foreignField": "context",
                        as: "status",
                    },
                },
                {
                    $lookup: {
                        from: "confirmed",
                        localField: "graphTxn.txid",
                        "foreignField": "tx.h",
                        as: "txc",
                    },
                },
                {
                    $lookup: {
                        from: "unconfirmed",
                        localField: "graphTxn.txid",
                        "foreignField": "tx.h",
                        as: "txu",
                    },
                },
                {
                    $match: {
                        $or: [
                            {"txc": {"$size": 1 }},
                            {"txu": {"$size": 1 }}
                        ]
                    }
                },
                { $project: { "graphTxn": 1, "status.bchBlockHeight": 1} },
            ],
            limit: 10,
        },
    };
    let b64 = Buffer.from(JSON.stringify(batonQuery)).toString("base64");
    console.log(`Fetching current minting baton location...`);
    // console.log(`SLPDB query: ${process.env.SLPDB_URL + b64}`);
    
    let graphResjson;
    while(!graphResjson) {
        const res = await fetch(process.env.SLPDB_URL + b64);
        graphResjson = await res.json();

        if (graphResjson.g === undefined) {
            graphResjson = undefined;
            await sleep(250);
            continue;
        } else if (graphResjson.g.length !== 1) {
            graphResjson = undefined;
            await sleep(250);
            // throw Error("Cannot find current contract tip!");
        }
    }

    const g = graphResjson.g[0];

    batonInputIndex = g.graphTxn.inputs.findIndex((i: any) => i.vout === 2);
    if (batonInputIndex !== 0 && !batonInputIndex) {
        throw Error("Cannot verify index of the previous baton input.");
    }
    lastBatonTxid = g.graphTxn.txid;
    mintVaultAddressT0 = g.graphTxn.outputs.find((o: any) => o.status === "BATON_UNSPENT").address;
    // bchBlockHeight = g.status[0].bchBlockHeight;

    // make sure we didn't fetch the same baton txid as previous
    if(lastBatonTxid === prev_lastBatonTxid) {
        console.log("We did not find new lastBatonTxid in SLPDB, retrying in 1 second");
        await sleep(1000);
        return {};
    }
    prev_lastBatonTxid = lastBatonTxid;
    
    console.log(`Current baton location: ${lastBatonTxid}:2`);

    // examine the transaction to determine the current state
    const rewardQuery = {
        v: 3,
        q: {
            db: ["u", "c"],
            find: {
                "tx.h": lastBatonTxid,
            },
            limit: 10,
        },
    };

    let txn: any;
    b64 = Buffer.from(JSON.stringify(rewardQuery)).toString("base64");
    // console.log(`Fetching SLP previous minting baton scriptSig info...`);
    // console.log(`SLPDB query: ${process.env.SLPDB_URL + b64}`);
    let res;
    let confResJson;
    while(!res) {
        try {
            res = await fetch(process.env.SLPDB_URL + b64);
        } catch (_) {
            await sleep(100);
            continue;
        }
    }
    confResJson = await res.json();
    if (confResJson.c.length === 1) {
        txn = confResJson.c[0];
    } else if (confResJson.u.length === 1) {
        txn = confResJson.u[0];
    } else {
        console.log("Waiting for SLPDB to update...");
        // start over
        return {};
    }

    let bestTokenHeight: number;
    let stateT0: string = txn.in[batonInputIndex].h0;
    if (stateT0.length > 8 && txn.in[batonInputIndex].e.h === process.env.TOKEN_ID_V1) {
        bestTokenHeight = 0;
        stateT0 = "00000000";
    } else if (stateT0.length === 8) {
        const buf = Buffer.from(stateT0, "hex");
        bestTokenHeight = buf.readInt32LE(0);
    } else {
        throw Error("Unhandled exception.");
    }

    console.log(`Current Mist height: ${bestTokenHeight}`);

    // verify actual t0 address matches our computed address
    const encodeAsHex = (n: number) => BITBOX.Script.encode([BITBOX.Script.encodeNumber(n)]).toString("hex");
    const initialMintAmount = encodeAsHex(parseInt(process.env.TOKEN_INIT_REWARD_V1 as string, 10));
    const difficultyLeadingZeroBytes = encodeAsHex(parseInt(process.env.MINER_DIFFICULTY_V1 as string, 10));
    const halvingInterval = encodeAsHex(parseInt(process.env.TOKEN_HALVING_INTERVAL_V1 as string, 10));
    const startingBlockHeight = encodeAsHex(parseInt(process.env.TOKEN_START_BLOCK_V1 as string, 10));
    const mintVaultHexT0 = `04${stateT0}20${process.env.TOKEN_ID_V1}${initialMintAmount}${difficultyLeadingZeroBytes}${halvingInterval}${startingBlockHeight}${vaultHexTail}`;

    const redeemScriptBufT0 = Buffer.from(mintVaultHexT0, "hex");
    const vaultHash160 = BITBOX.Crypto.hash160(redeemScriptBufT0);
    const vaultAddressT0 = Utils.slpAddressFromHash160(vaultHash160, "mainnet", "p2sh");
    // console.log(`T0 redeemScript:\n${mintVaultHexT0}`);
    const scriptPubKeyHexT0 = "a914" + Buffer.from(bchaddr.decodeAddress(vaultAddressT0).hash).toString("hex") + "87";
    // console.log(`T0 scriptPubKey:\n${scriptPubKeyHexT0}`);

    if (mintVaultAddressT0 !== vaultAddressT0) {
        throw Error("Mismatch contract address for t0, unknown error.");
    }

    // build t1 state
    const nextTokenHeight = bestTokenHeight + 1;
    const stateT1Buf = Buffer.alloc(4);
    stateT1Buf.writeInt32LE(nextTokenHeight, 0);
    const stateT1 = stateT1Buf.toString("hex");

    // construct the t1 contract
    const mintVaultHexT1 = `04${stateT1}20${process.env.TOKEN_ID_V1}${initialMintAmount}${difficultyLeadingZeroBytes}${halvingInterval}${startingBlockHeight}${vaultHexTail}`;
    const redeemScriptBufT1 = Buffer.from(mintVaultHexT1, "hex");
    const vaultHash160T1 = BITBOX.Crypto.hash160(redeemScriptBufT1);
    const vaultAddressT1 = Utils.slpAddressFromHash160(vaultHash160T1, "mainnet", "p2sh");
    // console.log(`T1 redeemScript:\n${mintVaultHexT1}`);
    const scriptPubKeyHexT1 = "a914" + Buffer.from(bchaddr.decodeAddress(vaultAddressT1).hash).toString("hex") + "87";
    // console.log(`T1 scriptPubKey:\n${scriptPubKeyHexT1}`);

    // get unspent UTXOs
    // console.log(`Getting unspent txos for ${minerBchAddress}`);
    const unspent = await client.getAddressUtxos({ address: minerBchAddress, includeMempool: true });
    const txos = unspent.getOutputsList().map((o) => {
        return {
            cashAddress: minerBchAddress,
            satoshis: o.getValue(),
            txid: Buffer.from(o.getOutpoint()!.getHash_asU8().reverse()).toString("hex"),
            vout: o.getOutpoint()!.getIndex(),
            wif: process.env.WIF,
            scriptPubKey: Buffer.from(o.getPubkeyScript_asU8()).toString("hex"),
        } as SlpAddressUtxoResult;
    });

    // console.log(`Completed fetching txos for ${minerBchAddress}`);

    // validate and categorize unspent TXOs
    // @ts-ignore
    // console.log(`Validating Mist transactions...`);
    const utxos = await network.processUtxosForSlp(txos);
    // console.log(`Finished validating Mist transactions...`);

    let txnInputs = utxos.nonSlpUtxos.filter((o: any) => o.satoshis >= 1870);

    if (txnInputs.length === 0) {
        throw Error("There are no non-SLP inputs available to pay for gas");
    }

    // add p2sh baton input with scriptSig
    const txo = {
        txid: lastBatonTxid,
        vout: 2,
        satoshis: 546,
    };
    // @ts-ignore
    const baton = await network.processUtxosForSlp([txo]);   
    
    // Save TxCache to file
    let txCacheJson = mapToJson(TxCache.txCache);
    console.log("Writing TxCache to JSON file...");
    writeJsonToFile(txCacheJson);
    console.log("Done");

    // select the inputs for transaction
    txnInputs = [ ...baton.slpBatonUtxos[process.env.TOKEN_ID_V1 as string], txnInputs[0] ];

    // construct the mint transaction preimage
    let extraFee = redeemScriptBufT0.length + 8 + 32 + 8 + 8 + 72 + 100;

    // Add 1 to bestTokenHeight when calculating reward
    // this is to fix OP_EQAULVERIFY error when block 4320 == 200 MIST, block 8640 == 133.33 MIST, etc
    const rewardAmount = getRewardAmount(bestTokenHeight + 1);

    // create a MINT Transaction
    let unsignedMintHex = txnHelpers.simpleTokenMint({
                    tokenId: process.env.TOKEN_ID_V1!,
                    mintAmount: new BigNumber(rewardAmount),
                    inputUtxos: txnInputs,
                    tokenReceiverAddress: minerSlpAddress,
                    batonReceiverAddress: vaultAddressT1,
                    changeReceiverAddress: minerSlpAddress,
                    extraFee,
                    disableBchChangeOutput: true,
    });

    // set nSequence to enable CLTV for all inputs, and set transaction Locktime
    unsignedMintHex = txnHelpers.enableInputsCLTV(unsignedMintHex);

    bchBlockHeight = useRpc ?
        (await rpc.getBlockchainInfo()).headers:
        (await client.getBlockchainInfo()).getBestHeight();

    console.log(`Blockchain height: ${bchBlockHeight}`);
    if((bchBlockHeight - TOKEN_START_BLOCK) > bestTokenHeight) {
        unsignedMintHex = txnHelpers.setTxnLocktime(unsignedMintHex, bchBlockHeight);
    } else {
        unsignedMintHex = txnHelpers.setTxnLocktime(unsignedMintHex, bchBlockHeight + 1);
    }

    // Build scriptSig
    const batonTxo = baton.slpBatonUtxos[process.env.TOKEN_ID_V1!][0];
    const batonTxoInputIndex = 0;
    const sigObj = txnHelpers.get_transaction_sig_p2sh(
                    unsignedMintHex,
                    minerWif,
                    batonTxoInputIndex,
                    batonTxo.satoshis,
                    redeemScriptBufT0,
                    redeemScriptBufT0,
                    );

    const tx = Bitcore.Transaction.fromHex(unsignedMintHex);
    let scriptPreImage: Buffer = tx.sigHashPreimageBuf(0, redeemScriptBufT0, 546, 0x41);

    // mine for the solution
    let prehash = Buffer.concat([scriptPreImage, crypto.randomBytes(4)]);
    let solhash = BITBOX.Crypto.hash256(prehash);
    let count = 0;

    console.log(`Mining height: ${bestTokenHeight + 1} (baton txid: ${lastBatonTxid})`);
    console.log("Starting workers to mine for solution");

    let workerMessage = {solhash, scriptPreImage, prehash};
    
    // get solution from worker when message received
    cluster.on('message', async (worker: any, message: any) => {
        solhash = message.solhash;
        scriptPreImage = message.scriptPreImage
        prehash = message.prehash;
        console.log(`Our worker solhash is: ${solhash.toString("hex")}`);

        solhashFound = true;
    });

    // send work to workerThread when online
    cluster.on('online', async (worker: any) => {
        worker.send(workerMessage);
    });

    // spawn workers
    for(let i = 0; i < minerCpuCount; i++) {
        let worker = cluster.fork();
    }

    console.log(`${minerCpuCount} workers are mining, please wait...`);

    // wait for solution from worker process
    while (!solhashFound) {

        /**
         *    sse is broken... don't use
         *
         */
        /*
        // sse early exit so we can try again
        if (mintFound) {
            console.log(`Token reward has been found, solution forfeited (on sse).`);
            for(const id in cluster.workers) {
                let currentWorker = cluster.workers[id];
                currentWorker.process.kill(currentWorker.pid);
            }
            return {};
        }
        */

        // check if baton spent after 1 second
        if(count >= 1000) {
            try {
                await client.getUnspentOutput({ hash: lastBatonTxid!, vout: 2, reversedHashOrder: true, includeMempool: true });
            } catch (_) {
                console.log(`Token reward has been found, solution forfeited (on interval check).`);
                // kill all workers before return
                for(const id in cluster.workers) {
                    let currentWorker = cluster.workers[id];
                    currentWorker.process.kill(currentWorker.pid);
                }

                return {};
                
            }

            count = 0;   
        }

        count++;

        // small delay before next check
        await sleep(1);
    }

    // kill all workers after solhash found
    for(const id in cluster.workers) {
        let currentWorker = cluster.workers[id];
        currentWorker.process.kill(currentWorker.pid);
    }

    const mintAmountLE = Buffer.alloc(4);
    mintAmountLE.writeUInt32LE(rewardAmount, 0);

    const scriptSigsP2sh = {
        index: batonTxoInputIndex,
        lockingScriptBuf: redeemScriptBufT0,
        unlockingScriptBufArray: [
            stateT1Buf,
            prehash.slice(scriptPreImage.length),
            // Buffer.from("2202000000000000", "hex"),
            mintAmountLE,
            sigObj.signatureBuf,
            minerPubKey,
            scriptPreImage,
            Buffer.from((process.env.MINER_UTF8 as string), "utf8"),
        ],
    } as ScriptSigP2SH;

    // Build p2pkh scriptSigs
    txnInputs[1].wif = process.env.WIF as string;
    const scriptSigsP2pkh = txnHelpers.get_transaction_sig_p2pkh(
        unsignedMintHex,
        minerWif,
        1, txnInputs[1].satoshis,
    ) as ScriptSigP2PKH;

    const scriptSigs = [ scriptSigsP2sh, scriptSigsP2pkh ] as Array<ScriptSigP2PK|ScriptSigP2PKH|ScriptSigP2SH>;
    const signedTxn = txnHelpers.addScriptSigs(unsignedMintHex, scriptSigs);

    // check if MINT BATON already spent
    try {
        await client.getUnspentOutput({ hash: lastBatonTxid!, vout: 2, reversedHashOrder: true, includeMempool: true });
    } catch (_) {
        console.log(`Token reward has been found, solution forfeited (post-mine forfeit).`);
        return {};
    }

    // get new bchBlockHeight in case of new block while mining
    bchBlockHeight = useRpc ?
        (await rpc.getBlockchainInfo()).headers:
        (await client.getBlockchainInfo()).getBestHeight();

    // try to send tx immediately if possible
    if((bchBlockHeight - TOKEN_START_BLOCK) > bestTokenHeight) {
        try {
            // use RPC if configured, otherwise use gRPC
            let result = useRpc ? 
                await rpc.sendRawTransaction(signedTxn):
                await client.submitTransaction({txnHex: signedTxn});
            // reverse bytes if gRPC
            if(!useRpc) { result = Buffer.from(result.getHash_asU8().reverse()).toString("hex"); }
            // console.log(rpcResult);
            console.log(`Submitted solution in txid: ${result}`);
            // return { rpcResult, vaultAddressT1 };
            return {};
        } catch(error) {
            console.log(`Token reward has been found, solution forfeited (${error}).`);
            return {};
        }
    }

    // use zeroMQ to detect new block
    if(useZeromq) {
        // try to submit tx when new block found
        sock.on('message', async (topic: string, message: string) => {
            try {
                // use RPC if configured, otherwise use gRPC
                let result = useRpc ? 
                    await rpc.sendRawTransaction(signedTxn):
                    await client.submitTransaction({txnHex: signedTxn});
                // reverse bytes if gRPC
                if(!useRpc) { result = Buffer.from(result.getHash_asU8().reverse()).toString("hex"); }
                // console.log(rpcResult);
                console.log(`Submitted solution in txid: ${result}`);
            } catch(error) {
                console.log(`Token reward has been found, solution forfeited (${error}).`);
            }

            bchBlockFound = true;
        });

        // wait for new block
        console.log("Waiting for next BCH block before submit...");
        while(!bchBlockFound) {
            await sleep(1);
        }
    // use gRPC to detect new block
    } else {
        // wait for new block
        console.log("Waiting for next BCH block before submit...");
        while(!bchBlockFound) {
            bchBlockHeight = (await client.getBlockchainInfo()).getBestHeight();
            // try to submit tx if we have new block
            if((bchBlockHeight - TOKEN_START_BLOCK) > bestTokenHeight) {
                try {
                    // use RPC if configured, otherwise use gRPC
                    let result = useRpc ? 
                        await rpc.sendRawTransaction(signedTxn):
                        await client.submitTransaction({txnHex: signedTxn});
                    // reverse bytes if gRPC
                    if(!useRpc) { result = Buffer.from(result.getHash_asU8().reverse()).toString("hex"); }
                    // console.log(rpcResult);
                    console.log(`Submitted solution in txid: ${result}`);
                } catch(error) {
                    console.log(`Token reward has been found, solution forfeited (${error}).`);
                }

                bchBlockFound = true;
            // otherwise wait 1 second and check again
            } else { await sleep(1000); }

        }
    }
    
    // start again
    return {};
    
};
